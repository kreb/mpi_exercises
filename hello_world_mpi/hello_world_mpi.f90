PROGRAM hello_world_mpi

    USE mpi
    IMPLICIT NONE

    INTEGER :: size, rank, ilen, ierror, i
    INTEGER :: status(MPI_STATUS_SIZE)
    CHARACTER(LEN = 256) :: name, buffer

    ! MPI
    CALL MPI_Init(ierror)
    CALL MPI_Comm_Rank(MPI_COMM_WORLD, rank, ierror)
    CALL MPI_Comm_Size(MPI_COMM_WORLD, size, ierror)
    CALL MPI_Get_Processor_Name(name, ilen, ierror)

    IF (rank.EQ.0) THEN
        WRITE(*, "(A)") "Hello World!"
        WRITE(buffer, "(A, I2, A, I2, 3A)") TRIM("rank "), rank+1, TRIM(" of "), size, &
                                                TRIM(" ("), name(1:ilen), TRIM(")")
        PRINT *, TRIM(buffer)

        DO i = 1, size - 1
            CALL MPI_Recv(buffer, 256, MPI_CHARACTER, i, MPI_ANY_TAG, MPI_COMM_WORLD, status, ierror)
            PRINT *, TRIM(buffer)
        ENDDO    
    ELSE
        WRITE(buffer, "(A, I2, A, I2, 3A)") TRIM("rank "), rank+1, TRIM(" of "), size, &
                                                TRIM(" ("), name(1:ilen), TRIM(")")
        CALL MPI_Send(buffer, 256, MPI_CHARACTER, 0, 0, MPI_COMM_WORLD, ierror)
    ENDIF

    CALL MPI_Finalize(ierror)

END PROGRAM