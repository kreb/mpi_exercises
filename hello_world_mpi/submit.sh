#!/bin/sh
#BSUB -q hpc
#BSUB -J hw_mpi
#BSUB -n 8 # specify number of cores
#BSUB -W 10:00 # specify maximum wall clock time
##BSUB -R "span[hosts=1]" # reserve all cores on one node
#BSUB -R "span[ptile=1]" # reserve one core per node
#BSUB -R "rusage[mem=2GB]"
#BSUB -B # send notification at start
#BSUB -N # send notification at end
#BSUB -o hello_world.out

module add studio
module add mpi/3.1.3-oracle-12u6
mpirun -np ${LSB_DJOB_NUMPROC} ./hello_world.e