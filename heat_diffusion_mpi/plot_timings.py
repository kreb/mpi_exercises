#!/usr/bin/env python

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

def amdahl(N, f):
    amdahl = []
    for e in N:
        temp = float(1 + (e - 1)*f)
        amdahl.append(e/temp)
    return amdahl


df = pd.read_csv('ex14_hpc/timings.dat')
dnp = df.to_numpy()

sns.set_theme()
sns.set_style("white")
sns.set_palette("Set1")

N = np.arange(1,20,1)

# time and bandwidth subplots
fig, ax = plt.subplots()
ax.plot(dnp[:, 0], dnp[0,1]/dnp[:, 1], 'o')  
ax.plot(N,N, 'k-') 
ax.set_xlabel('processors [-]')
ax.set_ylabel('speed-up [-]')
ax.set_title("[MPI] Timings for heat diffusion")
ax.legend(["HPC timings", "Linear Scaling"])

plt.savefig("heat_timings.pdf")
plt.show()

fig, ax = plt.subplots()
ax.plot(dnp[:, 0], dnp[:, 2], 'o')  
ax.set_xlabel('processors [-]')
ax.set_ylabel('RMS [-]')
ax.set_title("[MPI] RMS error for heat diffusion")

plt.savefig("heat_rms.pdf")
plt.show()