PROGRAM main
   ! ------------------------------------------------- !
   !  Filename : diff2d_mpi.e                          !
   !  Version  : 0.7                                   !
   !  Author   : Kristian Ebstrup Jacobsen             !
   !  Created  : January 22, 2022                      !
   ! ------------------------------------------------- !

   USE mpi
   USE m_precision
   USE m_global
   USE m_init
   USE m_io
   USE m_diffuse
   USE m_timer
   IMPLICIT NONE

   ! define local field
   REAL(wp), DIMENSION(:,:), ALLOCATABLE :: lfield, sfield

   ! initialize time variables
   REAL(wp)     :: cpu_t1, cpu_t2, wall_t1, wall_t2, error
   INTEGER      :: k, nk, iostat, bctype, sfield_step, i, j

   ! MPI variables
   INTEGER :: np, rank, ierror         ! number of processors, processor rank, and error integer
   INTEGER :: nxl, nyl                 ! processor-local nx and ny
   INTEGER :: status(MPI_STATUS_SIZE)

   ! ------------------------------------------------- !
   ! MPI START                                         !
   ! ------------------------------------------------- !
   CALL MPI_Init(ierror)
   CALL check_iostat(ierror)

   CALL MPI_Comm_Rank(MPI_COMM_WORLD, rank, ierror)
   CALL check_iostat(ierror)

   CALL MPI_Comm_Size(MPI_COMM_WORLD, np, ierror)
   CALL check_iostat(ierror)

   ! ------------------------------------------------- !
   ! INITIALIZATION                                    !
   ! ------------------------------------------------- !
   IF (rank.EQ.0) THEN
      PRINT*, "# ___________ PROGRAM STARTED ___________ #"
      PRINT "(A)"
   ENDIF

   ! read input file
   CALL read_input()

   ! check for terminal inputs to continue field
   ! CALL continue()

   IF (benchmark) THEN
      nk = 10
   ELSE
      nk = 1
   ENDIF


   ! subdivide field sizes
   nxl = (nx / np)
   nyl = ny

   ! last subdivision handles remainder
   IF (rank.EQ.(np-1)) THEN
      nxl = nx - nxl*(np-1)
   ENDIF

   ! subdivison boundary conditions
   IF (np.GT.1) THEN
      IF (rank.EQ.0) THEN
         nxl = nxl + 1  ! one ghost layer (right)
         bctype = 1     ! left, top, bottom boundaries
      ELSEIF (rank.EQ.(np-1)) THEN
         nxl = nxl + 1  ! one ghost layer (left)
         bctype = 2     ! right, top, bottom boundaries
      ELSE
         nxl = nxl + 2  ! two ghost layers (left, right)
         bctype = 3     ! top, bottom boundaries
      ENDIF
   ELSE
      bctype = 0        ! left, right, top bottom boundaries
   ENDIF

   DO k = 1, nk
      IF (rank.EQ.0) THEN
         ! start timer
         wall_t1 = MPI_WTime()

         ! allocate global field
         CALL alloc( field, nx, ny)
         CALL alloc(sfield, nx, ny)
      ENDIF

      ! allocate and initialize local field
      CALL alloc(lfield, nxl, nyl)
      CALL init_field(lfield, temp_boundary, temp_init, bctype)

      ! ------------------------------------------------- !
      ! SIMULATION                                        !
      ! ------------------------------------------------- !
      CALL diffuse(lfield, nsteps, nxl, nyl, fstep, rank, np)

      IF (rank.EQ.0) THEN
         ! end timer
         wall_t2 = MPI_WTime()
         PRINT*, "elapsed wall clock time:", wall_t2 - wall_t1
      ENDIF
   ENDDO

   ! combine into global field
   IF (np.GT.1) THEN
      IF (rank.EQ.0) THEN
         field(1:nxl-1,:) = lfield
         DO k = 1, (np-1)
            ! Receive local fields
            IF (k.EQ.(np-1)) THEN
               !PRINT *, "END | EXPECTED SIZE =", (nx - (nx / np)*k)*nyl
               !PRINT *, "END |    INPUT SIZE =", SIZE(field((1 + (nx / np) * k):nx,:))

               CALL MPI_Recv(&
                  field((1 + (nx / np) * k):nx,:), &
                  (nx - (nx / np)*k)*nyl,&
                  MPI_DOUBLE_PRECISION, k, 2, MPI_COMM_WORLD, status, ierror)
            ELSE
               !PRINT *, "MID | EXPECTED SIZE =", (nxl-1)*nyl
               !PRINT *, "MID |    INPUT SIZE =", SIZE(field((1 + (nxl - 1) * k):((nxl - 1) * (k + 1)),:))

               CALL MPI_Recv(&
                  field((1 + (nxl - 1) * k):((nxl - 1) * (k + 1)),:), &
                  (nxl-1)*nyl,&
                  MPI_DOUBLE_PRECISION, k, 2, MPI_COMM_WORLD, status, ierror)
            ENDIF
         ENDDO
      ELSE
         ! Send local field
         IF (rank.EQ.(np-1)) THEN
            CALL MPI_Send(&
               lfield(2:nxl,:),&
               SIZE(lfield(2:nxl,:)),&
               MPI_DOUBLE_PRECISION, 0, 2, MPI_COMM_WORLD, ierror)
            !PRINT *, "END |     SEND SIZE =", SIZE(lfield(2:nxl,:))
         ELSE
            CALL MPI_Send(&
               lfield(2:nxl-1,:),&
               SIZE(lfield(2:nxl-1,:)),&
               MPI_DOUBLE_PRECISION, 0, 2, MPI_COMM_WORLD, ierror)
            !PRINT *, "MID |     SEND SIZE =", SIZE(lfield(2:nxl-1,:))
         ENDIF
      ENDIF
   ELSE
      DO j = 1, ny
         DO i = 1, nx
            field(i, j) = lfield(i, j)
         ENDDO
      ENDDO
   ENDIF

   ! RMS DIFFERENCE WITH SEQUENTIAL FIELD
   OPEN(80, FILE="res/n1000_seq.bin", FORM="UNFORMATTED")
   READ(80, IOSTAT=iostat) sfield_step, sfield
   CLOSE(80)
   IF (rank.EQ.0) THEN
      error = 0.0_wp
      DO j = 1, ny
         DO i = 1, nx
            error = error + (&
               field(i, j) - sfield(i, j)&
               ) ** 2.0
         ENDDO
      ENDDO
      error = ((1.0_wp / REAL(nx * ny, wp)) * error)**0.5
      PRINT *, "RMS = ", error
   ENDIF

   ! ------------------------------------------------- !
   ! EXTRACT FIELD(S)                                  !
   ! ------------------------------------------------- !
   IF (rank.EQ.0) THEN
      IF (save_field) THEN
         CALL extract_field(field, formatted=.TRUE., step=nsteps+fstep, output=field_out)
      ENDIF
      IF (save_bin) THEN
         CALL extract_field(field, formatted=.FALSE., step=nsteps+fstep, output=bin_out)
      ENDIF

      PRINT "(A)"
      PRINT*, "# ___________ PROGRAM SUCCESSFUL ___________ #"
   ENDIF

   ! ------------------------------------------------- !
   ! MPI END                                           !
   ! ------------------------------------------------- !
   CALL MPI_Finalize(ierror)

END PROGRAM main

