#!/bin/sh
#BSUB -n 24
#BSUB -o 24.out
#BSUB -q hpc
#BSUB -J hw_mpi
#BSUB -W 10:00 # specify maximum wall clock time
##BSUB -R "span[hosts=1]" # reserve all cores on one node
#BSUB -R "span[ptile=1]" # reserve one core per node
#BSUB -R "rusage[mem=2GB]"
#BSUB -B # send notification at start
#BSUB -N # send notification at end

module add studio
module add mpi/3.1.3-oracle-12u6
mpirun -np ${LSB_DJOB_NUMPROC} ./diff2d_mpi.e
